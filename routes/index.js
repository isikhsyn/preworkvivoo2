const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
    res.send("İşlemler için localhost:3000/jadepage yada localhost:3000/api/fruits urllerini kullanın.");
});

module.exports = router;
