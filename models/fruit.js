const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fruitSchema = new Schema({
   adi: {
       type: String,
       required: true
   },
    fruitid: Schema.Types.ObjectId,
    meyveid: {
       type: Number,
        required: true

    },
});
module.exports = mongoose.model('fruit', fruitSchema);